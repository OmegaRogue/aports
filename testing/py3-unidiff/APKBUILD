# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=py3-unidiff
pkgver=0.7.2
pkgrel=0
pkgdesc="Unified diff python parsing/metadata extraction library"
url="https://github.com/matiasb/python-unidiff"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/matiasb/python-unidiff/archive/refs/tags/v$pkgver.tar.gz
	fix-test-script.patch"
builddir="$srcdir/python-unidiff-$pkgver"

build() {
	python3 setup.py build
}

check() {
	sh run_tests.sh
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname
	install -m644 README.rst HISTORY AUTHORS \
		"$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
3a1e758ac06e414b31f090d603b010a53c31638cf78ddb8af87af82235309637aaf1ce77e0ba1eaaca978511e84ea81fbc078fb2933d616f1745a6a4d6fadb3d  py3-unidiff-0.7.2.tar.gz
3c42b36da7a549d59647a2a489cb85f45d5d51c839b206893e39f2bc5fc14917719e1aa5cd1042758c55bc7a218c63f1df3bb76c28c57724cc6a890ecafa64bc  fix-test-script.patch
"
